#include <stream9/libmount/table.hpp>

#include "namespace.hpp"

#include <stream9/libmount/cache.hpp>
#include <stream9/libmount/fs.hpp>
#include <stream9/libmount/iterator.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(table_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        mnt::table t;

        BOOST_TEST(t.size() == 0);
        BOOST_TEST(t.empty());
    }

    BOOST_AUTO_TEST_CASE(parse_fstab_)
    {
        mnt::table t;
        t.parse_fstab();

        BOOST_TEST(!t.empty());
    }

    BOOST_AUTO_TEST_CASE(parse_mtab_)
    {
        mnt::table t;
        t.parse_mtab();

        BOOST_TEST(!t.empty());
    }

    BOOST_AUTO_TEST_CASE(filter_mtab_)
    {
        mnt::table t;
        t.parse_mtab();

        auto pred = [](auto fs) {
            return fs.fstype() == "fuse.encfs"
                || (!fs.is_swaparea() && !fs.is_pseudofs())
                ;
        };

        namespace rng = std::ranges;
        for (auto fs: rng::views::filter(t, pred)) {
            std::cout << fs << std::endl;
        }
    }

    BOOST_AUTO_TEST_CASE(filter_fstab_)
    {
        mnt::table fstab;
        fstab.parse_fstab();

        mnt::table mtab;
        mtab.parse_mtab();
        BOOST_TEST(!mtab.empty());

        mnt::cache c;
        mtab.set_cache(c);

        auto is_userfs = [&](auto fs) {
            return fs.fstype() == "fuse.encfs"
                || (!fs.is_swaparea() && !fs.is_pseudofs())
                ;
        };

        namespace rng = std::ranges;
        for (auto fs: rng::views::filter(fstab, is_userfs)) {
            std::cout << "----------" << std::endl;
            std::cout << "source: " << *fs.source() << std::endl;
            std::cout << "target: " << *fs.target() << std::endl;
            std::cout << "fstype: " << *fs.fstype() << std::endl;
            std::cout << "mounted: " << mtab.is_fs_mounted(fs) << std::endl;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // table_

} // namespace testing
