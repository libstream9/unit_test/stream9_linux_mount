#ifndef STREAM9_LIBMOUNT_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LIBMOUNT_TEST_SRC_NAMESPACE_HPP

namespace stream9::libmount {}

namespace testing {

namespace mnt { using namespace stream9::libmount; }

} // namespace testing

#endif // STREAM9_LIBMOUNT_TEST_SRC_NAMESPACE_HPP
