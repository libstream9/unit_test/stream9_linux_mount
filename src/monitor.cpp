#include <stream9/libmount/monitor.hpp>

#include "namespace.hpp"

#include <stream9/libmount/tabdiff.hpp>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>

#include <iomanip>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::errors::print_error;
namespace json = stream9::json;

BOOST_AUTO_TEST_SUITE(monitor_)

    BOOST_AUTO_TEST_CASE(step1_)
    {
        try {
            mnt::monitor m;
            m.enable_kernel();
            m.enable_userspace();

            mnt::table mtab;
            mtab.parse_mtab();

            using namespace std::literals;

            while (m.wait(10s)) {
                while (auto o_c = m.next_change()) {
                    mnt::table new_mtab;
                    new_mtab.parse_mtab();

                    mnt::tabdiff diffs { mtab, new_mtab };

                    std::cout << "mount changed" << std::endl;
                    for (auto&& d: diffs) {
                        std::cout << std::setw(2) << d << std::endl;
                    }

                    mtab = new_mtab;
                }
            }
        }
        catch (...) {
            print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // monitor_

} // namespace testing
